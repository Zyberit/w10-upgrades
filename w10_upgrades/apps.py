from django.apps import AppConfig


class W10UpgradesConfig(AppConfig):
    name = 'w10_upgrades'
