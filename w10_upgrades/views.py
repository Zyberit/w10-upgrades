from django.shortcuts import render

# Create your views here.

import os, threading
from datetime import datetime
from django.http import HttpResponse
from .models import PageView
from .forms import CheckForm

compcash = {}
cash_age = None
DBX_FILE = os.getenv('DBX_FILE', 'NO DEFAULT!')
MAX_CASH_AGE = 3600     # seconds

import requests
from openpyxl import load_workbook
from io import BytesIO
def load_cash():
#     print ("load_cash!")
    global compcash, cash_age
    tmpcash = {}
    r = requests.get(DBX_FILE, allow_redirects=True, stream=True)
    wb = load_workbook(filename=BytesIO(r.raw.read()),read_only=True)
    # wb = load_workbook(filename=r.raw)

    ws = wb['Alla']
    colmap = {}
    for c in range(1,ws.max_column+1):
        colmap[ws.cell(row=1,column=c).value] = c
    
    for r in range(2,ws.max_row+1):
        byts = ws.cell(row=r,column=colmap['Byts']).value == "Ja"
        vecka = ws.cell(row=r,column=colmap['Vecka']).value
        if byts:
            v = -1
        else:
            if vecka is not None:
                v = int(vecka)
            else:
                v = 0
             
        tmpcash[ws.cell(row=r,column=colmap['Datornamn']).value] = v
#     threading.Lock.aquire()
    compcash = tmpcash
    cash_age = datetime.now()


def check(request):
    hostname = os.getenv('HOSTNAME', 'unknown')
    PageView.objects.create(hostname=hostname)

    if cash_age is None:
        load_cash()     # Must have an initial load!
        print("Inital load")
    if (datetime.now()-cash_age).seconds > MAX_CASH_AGE:
        print ("Threaded load: {}".format(datetime.now()-cash_age).seconds)
        t = threading.Thread(target=load_cash)
        t.start()       # Start colleting data in the background

    comp = request.GET['q']
    if comp in compcash:
        return HttpResponse(str(compcash[comp]))
    else:
        return HttpResponse("-2")

def index(request):
    hostname = os.getenv('HOSTNAME', 'unknown')
    PageView.objects.create(hostname=hostname)

    if cash_age is None:
        print("Inital load")
        load_cash()     # Must have an initial load!
    if (datetime.now()-cash_age).seconds > MAX_CASH_AGE:
        print ("Threaded load: {}".format(datetime.now()-cash_age).seconds)
        t = threading.Thread(target=load_cash)
        t.start()       # Start colleting data in the background
    
    status = ""
    if request.method == 'POST':
        form = CheckForm(request.POST)
        if form.is_valid():
            computer = form.cleaned_data['computer']
            if computer in compcash:
                v = compcash[computer]
                if v > 0:
                    status = "Din dator ska uppgraderas vecka {}.".format(v)
                if v == 0:
                    status = "Uppgradering av din dator är inte inplanerad än."
                if v < 0:
                    status = "Din dator ska inte uppgraderas utan bytas ut."
            else:
                status = "Din dator är okänd för systemet. Kontakta ServiceDesk för mer information."
    else:   # New form
        form = CheckForm()
    return render(request, 'check.html', {'form': form, 'status':status})

    
def health(request):
    return HttpResponse(PageView.objects.count())

