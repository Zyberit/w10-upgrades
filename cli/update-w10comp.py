'''
Created on 19 apr. 2019

@author: hakan
'''

from os import rename, listdir
import csv
from datetime import datetime
from openpyxl import load_workbook

def read_w10comps():
    w10comps=[]
    for fname in listdir("."):
        if ".csv" in fname:
#             print(fname)
            with open(fname) as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    w10comps.append(row['Namn'])
    return w10comps

def fix_xlsx(oldfile,newfile,w10comps):
    wb = load_workbook(filename=oldfile)
    ws = wb['Alla']
    colmap = {}
    for c in range(1,ws.max_column+1):
        colmap[ws.cell(row=1,column=c).value] = c
    for r in range(2,ws.max_row+1):
        if ws.cell(row=r,column=colmap['Datornamn']).value in w10comps:
            ws.cell(row=r,column=colmap['Bytt']).value = "Ja"
            print(ws.cell(row=r,column=colmap['Datornamn']).value)
    wb.save(filename=newfile)

def main():
    w10comps = read_w10comps()
    now = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    file1 = "Utrullningsplanering.xlsx"
    file2 = "Utrullningsplanering_{}.xlsx".format(now)
    rename(file1,file2)
    fix_xlsx(file2,file1,w10comps)
    


if __name__ == '__main__':
    main()